import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  { path: '' , loadChildren: './tabs/tabs.module#TabsPageModule' },
  {
    path: 'signup',
    loadChildren: () => import('./modals/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'view-content',
    loadChildren: () => import('./modals/view-content/view-content.module').then( m => m.ViewContentPageModule)
  },

 
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
