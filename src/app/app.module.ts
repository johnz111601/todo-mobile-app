import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { SignupPageModule } from './modals/signup/signup.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastService } from './providers/toast-service';
import { SharedGlobalService } from './providers/shared.global.services';
import { AuthService } from './providers/auth.services';
import { DatePipe } from '@angular/common';
import { ConnectionService } from './providers/connection.service';
import { ViewContentPageModule } from './modals/view-content/view-content.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SignupPageModule,
    ReactiveFormsModule,
    FormsModule,
    ViewContentPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ToastService,
    SharedGlobalService,
    AuthService,
    DatePipe,
    ConnectionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
