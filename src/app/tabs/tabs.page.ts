import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../providers/auth.services';
import { ToastService } from '../providers/toast-service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public alertController: AlertController,
    public auth: AuthService,
    public toastService: ToastService,) {
      if(this.auth.getUserData()){
        this.auth.login();
      } else {
        this.auth.logout();
      }
    }


 async logout(){
    console.log("logout");
    const alert = await this.alertController.create({
      header: 'Logout',
      message: 'are you sure you want to logout?',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.auth.logout();
            this.toastService.dynamicToast("Logged out Successfully", "", 3000);
          }
        }
      ]
    });
    await alert.present();
  }

}
