import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';

@Injectable()
export class AuthService {
  public user: any;
  public authToken: any;
  public options: any;
  public notificationInstance: any;
  public hasNotificationInstance: Boolean;

  public isOnline: boolean = false;

  constructor(
    public http: Http,
    public router: Router,
    public location: Location,
    public modalController: ModalController
  ) { }

  Headers(xdata?) {
    return xdata ?
      new RequestOptions({
        headers: new Headers({
          'Accept': 'application/json',
          'Authorization': this.getToken(),
          'xdata': xdata
        }),
      }) :
      new RequestOptions({
        headers: new Headers({
          'Accept': 'application/json',
          'Authorization': this.getToken(),
        }),
      });
  }

  /**
  * @param {String} data server payload / token
  * @return {Promise} Boolean true or false; setting tokens
  */
  public setToken(data) {
    // console.log("auth");
    return new Promise((resolve, reject) => {
      try {
        if (typeof (Storage) !== "undefined") {
          for (let prop in data) {
            let
              type = typeof data[prop],
              context = type.toLowerCase() === 'string' ?
                data[prop] : JSON.stringify(data[prop]);
            localStorage.setItem(prop, context)
          }
          resolve(true);
        } else {
          reject(false);
        }
      } catch (e) {
        console.info(e);
        reject(false);
      }
    });
  }

  /**
   * @return {String} token
   */
  public getToken() {
    return localStorage.getItem('token');
  }

  /**
   * @return {Boolean} if user has token and equals to role
   * true or false
   */
  public hasToken() {
    return this.getToken() ? (() => { this.Observer(); return true; })() : false;
  }

  /**
   * @return {Void} 
   * @description observes routes by role
   */
  public Observer(): void {
    // let role = this.getTokenData('role');
    // let self = this.router.events.subscribe(nextUrl => {
    //   if(nextUrl instanceof NavigationEnd){
    //     if(this.location.path().split('/')[1] !== role){
    //       self.unsubscribe();
    //       // this.router.navigate([role]);

    //       this.router.navigate(['/']);
    //     }
    //   }
    // });
  }

  /**
   * @param {String} key any object property of signed user
   * @return {String} key value
   */
  public getTokenData(key?) {
    try {
      if (this.hasToken) {
        let token = JSON.parse(atob(this.getToken().split('.')[1]));
        if (key) {
          switch (typeof key) {
            case 'string':
              return token[key]
            case 'object':
              return key.map(k => token[k]);
          }
        } else { return token; }
      }
    } catch (e) {
      this.logout();
    }
  }

  /**
  * @param {String} key any object property of signed user to replace
  * @param {String} val value to replace
  * @return {String} key value
  */
  public setUserData(key, val) {
    try {
      if (this.hasToken) {
        let user;
        user = JSON.parse(localStorage.getItem('user')),
          user[key] = val,
          user = JSON.stringify(user),
          localStorage.setItem('user', user);
      }
    } catch (e) {
      this.logout();
    }
  }

  /**
   * @param {String} key any object property of signed user
   * @return {String} key value
   */
  public getUserData(key?) {
    try {
      if (this.hasToken) {
        let token = JSON.parse(localStorage.getItem('user'));
        if (key) {
          switch (typeof key) {
            case 'string':
              return token[key]
            case 'object':
              return key.map(k => token[k]);
          }
        } else { return token; }
      }
    } catch (e) {
      this.logout();
      //console.info('Error:', e.message);
    }
  }



  /**
   * @return {Void}
   * @description clear token & user then navigate login
   */
  public logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');

    // this.hasNotificationInstance &&
    //   this.notificationInstance.unsubscribe(),
    //   this.hasNotificationInstance = false;

    this.router.navigate([``]);
    // window.location.reload(true);
  }

  public async login(role?) {
    console.log(role);
    console.log(this.getTokenData('role'));
    if (role == 'admin' || this.getTokenData('role') == 'admin') {
      this.router.navigate([`./tabs`]);
    } else {

    }
  }

}
