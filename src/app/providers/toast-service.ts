import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    constructor(public toastController: ToastController) {

    }

    async dynamicToast(header, message, duration) {
        const toast = await this.toastController.create({
            header: header,
            message: message,
            duration: duration,
            mode: 'ios',
            cssClass: 'toast-ctrl',
        });
        toast.present();
    }

}
