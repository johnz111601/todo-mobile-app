import { Injectable, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from './auth.services';
import { DatePipe,Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { LoadingController, ToastController } from '@ionic/angular';
import { ConnectionService } from './connection.service';
// import { Observable } from 'rxjs/internal/Observable';
import * as io from 'socket.io-client';
import * as md5 from 'md5';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';



@Injectable()
export class SharedGlobalService {
    

    public connection = this.cs.connection; 
    public server: any = { status: true, message: "online" };
    public notificationStatus: Boolean = false;
    public loading: Boolean = false;
    public modalLoading: Boolean = true;
    public selectorLoader: Boolean = false;
    public submitting: Boolean = false;
    public notifLength: any = 0;
    public bypassAttempt: Number = 0;
    public selectedActivity: any;
    public dummy:any;
    public calendar: any;
    public gloabalLoading;
    public globalToaster;
    public theme: any = {};
    public items: any = [];
    
    @Output() public themeEmitter = new EventEmitter();
    @Output() public cacheEmitter = new EventEmitter();
    @Output() public ionToastEmitter = new EventEmitter();
    @Output() public onLogin = new EventEmitter();
    @Output() public tabsActive = new EventEmitter();
    public currentPage: string = '';
    public courierTransactions: any;
    public courierDeliveredTransactions: any;
    public availableTransactions: any;
    public availableTransactionsBadge: number = 0;
    public tabPage = '';

    account: any = [] // temp variable container from component to component that contains [id, name, filters]

    constructor(
        public http: Http,
        public router: Router,
        public auth: AuthService,
        public location: Location,
        public dp: DatePipe,
        public browserTitle: Title,
        public loadingController: LoadingController,
        public toastController: ToastController,
        public cs: ConnectionService,
        // public localNotifications: LocalNotifications
    ) {

        this.ionToastEmitter.subscribe(async res => {
            if (res.success) {
                this.globalToaster = await this.toastController.create({
                    message: res.data.message,
                    // showCloseButton: true,
                    duration: 5000,
                    color: res.data.type
                });
                await this.globalToaster.present();
            }
        });

    }

    /**
     * @param {String} method get, post, put
     * @param {String} endpoint API model/route
     * @param {Any} param  JSON data
     * @param {Function} callback returns callback
     * @return {Object} subscribed data
     */
    public request(method: String, endpoint: String, param?: any, callback?: any, options?: any) {
        let connection = this.connection + '/api';

        switch (method) {
            case 'get':
                this.StartLoading(options);
                param = param ? typeof param === 'object' ? JSON.stringify(param) : undefined : undefined;
                return this.http.get(`${connection}/${endpoint}`, this.auth.Headers(param))
                    .map(res => res.json())
                    .subscribe(res => {
                        if (res.success) {
                            this.StopLoading();
                            return callback(res);
                        } else {
                            this.StopLoading();
                            return this.fail(res, callback, options);
                        }
                    }, error => {
                        this.StopLoading();
                        this.RequestCache(method, endpoint, param, options);
                        this.ionToastEmitter.emit({
                            success: true,
                            data: {
                                message: `Server Error: ${error}`,
                                type: 'danger'
                            }
                        });
                    });

            case 'post':
                this.StartLoading(options);
                return this.http.post(`${connection}/${endpoint}`, param, this.auth.Headers())
                    .map(res => res.json())
                    .subscribe(async res => {
                        if (res.success) {
                            await this.StopLoading();
                            return callback(res);
                        } else {
                            await this.StopLoading();
                            return this.fail(res, callback, options);
                        }
                    }, error => {
                        this.StopLoading();
                        this.RequestCache(method, endpoint, param, options);
                        this.ionToastEmitter.emit({
                            success: true,
                            data: {
                                message: `Server Error: ${error}`,
                                type: 'danger'
                            }
                        });
                    });

            case 'put':
                this.StartLoading(options);
                return this.http.put(`${connection}/${endpoint}`, param, this.auth.Headers())
                    .map(res => res.json())
                    .subscribe(res => {
                        if (res.success) {
                            this.StopLoading();
                            return callback(res);
                        } else {
                            this.StopLoading();
                            return this.fail(res, callback, options);
                        }
                    }, error => {
                        this.StopLoading();
                        this.RequestCache(method, endpoint, param, options);
                        this.ionToastEmitter.emit({
                            success: true,
                            data: {
                                message: `Server Error: ${error}`,
                                type: 'danger'
                            }
                        });
                    });
        }
    }

    public back() {
        this.location.back();
    }


    public goto(location) {
        this.router.navigate([location]);
    }

    public StartLoading(options?) {
        if (!this.submitting) {
            this.loading = true;
            this.modalLoading = true;
        }

        for (let prop in options) {
            switch (prop) {
                case 'socketLoader':
                    this.loading = options[prop] === true ? true : false;
                    this.modalLoading = options[prop] === true ? true : false;
                    break;
                case 'socketModalLoader':
                    this.modalLoading = options[prop] === true ? true : false;
                    break;
                case 'selectorLoader':
                    this.selectorLoader = options[prop] === true ? true : false;
                    break;
            }
        }

    }

    public StopLoading() {
        this.loading = false;
        this.submitting = false;
        this.modalLoading = false;
        this.selectorLoader = false;
    }

    public fail(res, callback, options?: any) {
        if (res.codeMessage !== undefined) {
            this.kill(res);
        } else {
            let toast = (() => {
                this.ionToastEmitter.emit({
                    success: true,
                    data: {
                        message: (() => {
                            if (typeof res.message === 'object')
                                return JSON.stringify(res.message)
                            return res.message;
                        })(),
                        type: 'danger'
                    }
                });
            });
            options ? options.hasOwnProperty('toaster') ? options.toaster ? toast() : '' : toast() : toast();
            return callback(res);
        }
    }

    public kill(data) {
        this.ionToastEmitter.emit({
            success: true,
            data: {
                message: `Error: ${data.message} ${data.codeMessage}`,
                type: 'danger'
            }
        });
        console.info(data.message, data.codeMessage);
        this.auth.logout();
    }


    public getNotificationLength() {
        this.request('get', 'notification/getNotificationLength', 'empty', async res => {
            if (res.success) {
                this.notifLength = res.data.count;
            } else {
                this.notifLength = 0;
            }
        }, { toaster: false, socketLoader: false });
    }


    public RequestCache(method, endpoint, param, options) {
        if (options) {
            for (var opt in options) {
                if (opt === 'cache' && options[opt] === true) {
                    let data = {
                        cache_id: md5([Math.random(), Math.random(), (new Date()).getMilliseconds()].join()),
                        uid: this.auth.getTokenData('id'),
                        user_role: this.auth.getTokenData('role'),
                        method: method,
                        endpoint: endpoint,
                        param: param,
                        routes: this.router.url,
                        describe: options['describe'] ? options['describe'] : '',
                        cache_date: this.dp.transform(new Date(), 'medium')
                    };
                    this.InsertCache(data);
                }
            }
        }
    }

    public CacheManager() { }
    public syncData() { }

    public InsertCache(data) {
        try {
            let rc = JSON.parse(localStorage.getItem('request-cache')) || [];
            if (rc.length) {
                let tempData = Object.assign({}, data);
                let isRecordExists = false;
                rc.map(record => {
                    let tempRecord = Object.assign({}, record);
                    tempRecord.cache_date = tempData.cache_date =
                        tempRecord.cache_id = tempData.cache_id = undefined;
                    if (JSON.stringify(tempRecord) === JSON.stringify(tempData)) {
                        isRecordExists = true;
                        tempRecord = tempData = undefined;
                    }
                });
                if (!isRecordExists) {
                    rc.push(data);
                    localStorage.setItem('request-cache', JSON.stringify(rc));
                    rc = undefined;
                    this.cacheEmitter.emit();
                }
            } else {
                localStorage.setItem('request-cache', JSON.stringify([data]));
                this.cacheEmitter.emit();
            }
        } catch (e) {
            this.ionToastEmitter.emit({
                success: true,
                data: {
                    message: `Error: ${e.name} ${e.message}`,
                    type: 'danger'
                }
            });
            localStorage.removeItem('request-cache');
        }
    }

    public SetCachedData(data) {
        localStorage.setItem('request-cache', JSON.stringify(data));
    }

    public getCachedData(options?: any) {
        try {
            let rc = JSON.parse(localStorage.getItem('request-cache')) || [];
            let uid = this.auth.getTokenData('id');
            return rc.filter(record => record.uid === uid).reverse();
        } catch (e) {
            this.ionToastEmitter.emit({
                success: true,
                data: {
                    message: `Error: ${e.name} ${e.message}`,
                    type: 'danger'
                }
            });
            localStorage.removeItem('request-cache');
        }
    }

    public removeCachedData(id) {
        try {
            let rc = JSON.parse(localStorage.getItem('request-cache')) || [];
            let uid = this.auth.getTokenData('id');
            if (rc.length) {
                rc.map((record, index) => {
                    if (record.cache_id === id && record.uid === uid) {
                        rc.splice(index, 1)
                    }
                });
                this.SetCachedData(rc);
                this.cacheEmitter.emit();
            }
        } catch (e) {
            this.ionToastEmitter.emit({
                success: true,
                data: {
                    message: `Error: ${e.name} ${e.message}`,
                    type: 'danger'
                }
            });
            localStorage.removeItem('request-cache');
        }
    }

    public getTime(format?: string) {
        let date = new Date();
        switch (format) {
            case 'hh':
                return [date.getHours()].join('');
            case 'mm':
                return [date.getMinutes()].join('');
            case 'hhmm':
                return [date.getHours(), date.getMinutes()].join('');
            case 'hhmmss':
                return [date.getHours(), date.getMinutes(), date.getSeconds()].join('');
            default:
                return [date.getHours(), date.getMinutes()].join('');
        }
    }


    ResponseSocket(name?) {
        let socket = io.connect(this.connection);
        return new Observable(observer => {
            socket.on(name, (data) => {
                this.server.status = true;
                this.server.message = "online";
                observer.next(data);
            });

            socket.on('reconnect_error', () => {
                // console.clear();
                this.server.status = false;
                this.server.message = "offline";
            });

            socket.on('reconnect', () => {
                // console.clear();
                this.server.status = true;
                this.server.message = "online";
            });

            return () => {
                socket.disconnect();
            };
        }) as any;
    }

    RequestSocket(name?, data?) {
        let socket = io.connect(this.connection);
        socket.emit(name, data);
    }

    Lan() {
        try {
            return { status: navigator.onLine, message: navigator.onLine ? "online" : "offline" };
        } catch (e) {
            console.error("Browser doesn't support navigation status");
            return { status: true, message: "Browser doesn't support navigation status" };
        }
    }

    GetImagePath(image) { }

    public removeItem(arr, arrId, id) {
        return arr.filter((result, index) => {
            if (result[arrId] === id) {
                arr.splice(index, 1);
            }
        });
    }
}