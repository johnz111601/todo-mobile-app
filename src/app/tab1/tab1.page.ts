import { Component, NgZone } from '@angular/core';
import { SharedGlobalService } from '../providers/shared.global.services';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { ToastService } from '../providers/toast-service';
import { ViewContentPage } from '../modals/view-content/view-content.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public responseSocketInstance: any;
  public todos: any = [];

  constructor(public sgs: SharedGlobalService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public toastService: ToastService,
    public modalController: ModalController,
    public zone: NgZone) {

    this.responseSocketInstance = sgs.ResponseSocket('todo').subscribe(emitted => {
      this.getAllUserTodo();
    });
    this.getAllUserTodo();

  }



  async addToDo() {
    const alert = await this.alertController.create({
      header: 'Add Task',
      mode: "ios",
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'Title'
        },
        {
          name: 'note',
          type: 'textarea',
          placeholder: 'Note'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Add',
          handler: async (data) => {
            if (data) {
              const loading = await this.loadingController.create({
                message: 'Please wait...',
                duration: 10000
              });
              await loading.present();
              this.sgs.request('post', 'todo/addTodo', data, async res => {
                if (res.success) {
                  await loading.dismiss();
                  this.toastService.dynamicToast("Success", "Added Successfully", 3000);
                }
              })
            }
          }
        }
      ]
    });
    await alert.present();
  }

  getAllUserTodo() {
    this.sgs.request('get', 'todo/getAllTodo', { status: 1 }, async res => {
      if (res.success) {
        if (res.data.length) {
          this.todos = res.data;
        } else {
          this.todos = [];
        }
        this.zone.run(() => {
          this.todos
        })
      }
    })
  }

  async view(data) {
    const modal = await this.modalController.create({
      component: ViewContentPage,
      componentProps: {
        data: data
      }
    });
    return await modal.present();
  }

  async delete(data) {
    const alert = await this.alertController.create({
      header: 'Move to trash',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toTrash', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Removed", "Task moved to trash.", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async done(data) {
    const alert = await this.alertController.create({
      header: "Done taks",
      message: "To confirm the data will move to archive.",
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toArchive', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Done", "You have done another task, Good job.", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

}
