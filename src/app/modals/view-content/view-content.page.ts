import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from 'src/app/providers/shared.global.services';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-view-content',
  templateUrl: './view-content.page.html',
  styleUrls: ['./view-content.page.scss'],
})
export class ViewContentPage implements OnInit {

  data: any;

  title: any;
  content: any;
  constructor(public sgs: SharedGlobalService,
    public modalController: ModalController) { }

  ngOnInit() {
    console.log(this.data);
  }


  async update(){
    console.log(this.data);
    this.sgs.request('put', 'todo/updateTodo', this.data , async res => {
      if (res.success) { }
    })
  }

  close(){
    this.modalController.dismiss();
  }

}
