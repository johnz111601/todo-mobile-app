import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ToastService } from 'src/app/providers/toast-service';
import { SharedGlobalService } from 'src/app/providers/shared.global.services';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  forms: FormGroup;

  constructor(public modalController: ModalController,
    public formBuilder: FormBuilder,
    public toastService: ToastService,
    public sgs: SharedGlobalService,
  ) {

  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.forms = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      retypePassword: ['', [Validators.required]],
    });
  }

  signup() {
    if(this.forms.value['password'] == this.forms.value['retypePassword']){
      console.log(this.forms.value)
      this.sgs.request('post', 'user/createUser', this.forms.value, res => {
        console.log(res);
        if(res.success){
          this.modalController.dismiss();
          this.toastService.dynamicToast("Registered Successfully", "", 3000);
        } else {
          // this.toastService.dynamicToast("Username Invalid", "Username already exist.", 3000);
        }
      })
    }else {
      this.toastService.dynamicToast("Invalid", "Password and Re-type password not match.", 3000);
    }
  }

  async close() {
    this.modalController.dismiss();
  }
}
