import { Component, OnInit, NgZone } from '@angular/core';
import { SharedGlobalService } from '../providers/shared.global.services';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { ToastService } from '../providers/toast-service';
import { ViewContentPage } from '../modals/view-content/view-content.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  public responseSocketInstance: any;
  public todos: any = [];

  constructor(public sgs: SharedGlobalService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public toastService: ToastService,
    public modalController: ModalController,
    public zone: NgZone) {

    this.responseSocketInstance = this.sgs.ResponseSocket('todo').subscribe(emitted => {
      this.getAllUserTodoTrash();
    });
  }

  ngOnInit() {
    this.getAllUserTodoTrash();
  }

  async getAllUserTodoTrash() {
    this.sgs.request('get', 'todo/getAllTodo', { status: 3 }, async res => {
      if (res.success) {
        if (res.data.length) {
          this.todos = res.data;
        } else {
          this.todos = [];
        }
        this.zone.run(() => {
          this.todos
        })
      }
    })
  }

  async moveToArchive(data) {
    const alert = await this.alertController.create({
      header: 'Move to Archive',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toArchive', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Moved to Archive", "", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async moveToTask(data) {
    const alert = await this.alertController.create({
      header: 'Move to Task',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toTask', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Moved to Task", "", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

 async view(data){
    const modal = await this.modalController.create({
      component: ViewContentPage,
      componentProps: {
        data: data
      }
    });
    return await modal.present();
  }

  async permanentDelete(data?){
    const alert = await this.alertController.create({
      header: 'Permanently Delete.',
      message: 'are you sure you want to delete it permanently?',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/deletePermanent', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Permanently Deleted.", "", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

}
