import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SignupPage } from '../modals/signup/signup.page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedGlobalService } from '../providers/shared.global.services';
import { AuthService } from '../providers/auth.services';
import { ToastService } from '../providers/toast-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  forms: FormGroup;
  username: any;
  password: any;

  constructor(public modalController: ModalController,
    public formBuilder: FormBuilder,
    public sgs: SharedGlobalService,
    public auth: AuthService,
    public toastService: ToastService,) { }

  ngOnInit() {
    console.log(this.auth.getUserData());
    if(this.auth.getUserData()){
      this.auth.login();
    } else {
      this.auth.logout();
    }
    this.createForm();
  }

  createForm() {
    this.forms = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

 async signin(){
    this.sgs.request('post', 'user/authenticate', this.forms.value,async res => {
      if(res.success){
        this.auth.setToken(res.data);
        this.auth.login(res.data.user.role);
        this.toastService.dynamicToast("Login Successfully", "", 3000);
      } else {
        // this.toastService.dynamicToast("Login Failed", "Check Credentials", 3000);
      }
    })
  }

  async signup(){
    const modal = await this.modalController.create({
      component: SignupPage
    });
    return await modal.present();
  }

}
