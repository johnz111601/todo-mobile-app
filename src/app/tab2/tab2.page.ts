import { Component, OnInit, NgZone } from '@angular/core';
import { SharedGlobalService } from '../providers/shared.global.services';
import { ViewContentPage } from '../modals/view-content/view-content.page';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { ToastService } from '../providers/toast-service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  public responseSocketInstance: any;
  public todos: any = [];

  constructor(public sgs: SharedGlobalService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public toastService: ToastService,
    public modalController: ModalController,
    public zone: NgZone)
   {
    this.responseSocketInstance = this.sgs.ResponseSocket('todo').subscribe(emitted => {
      this.getAllUserTodoArchive();
     
    });
  }

  ngOnInit() {
   this.getAllUserTodoArchive();
  }


  async getAllUserTodoArchive() {
    this.sgs.request('get', 'todo/getAllTodo', {status: 2}, async res => {
      if (res.success) {
        if (res.data.length) {
          this.todos = res.data;
        } else {
            this.todos = [];
        }
        this.zone.run(()=>{
          this.todos
        })
      }
    })
  }

  async view(data){
    const modal = await this.modalController.create({
      component: ViewContentPage,
      componentProps: {
        data: data
      }
    });
    return await modal.present();
  }

  async returnToTask(data){
    const alert = await this.alertController.create({
      header: "Move to Task",
      message: "To confirm the data will move to task list.",
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toTask', data , async res => {
              if (res.success) {
             
                this.toastService.dynamicToast("Move to Task","Task moved to list.", 3000);
               }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async delete(data) {
    const alert = await this.alertController.create({
      header: 'Move to trash',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.sgs.request('put', 'todo/toTrash', data, async res => {
              if (res.success) {
                this.toastService.dynamicToast("Removed", "Task moved to trash.", 3000);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

}
